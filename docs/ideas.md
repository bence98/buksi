# Ideas

# Config generation
The i3 config is a personal thing.
But some functionality has a lot in common.

# Screen locking
- time factor
- locking screen
  - color
  - image
  - blur

# Energy
Some energy intensive apps:
- docker
- web server
- database

# Network interfaces
- WiFi not always needed
- Ethernet interfaces?
- VPN?

# Bluetooth
- like wifi
- auto-conenct to headphone

# Profiles
- SCH910
  - on power
  - lock time:2p
- portable
  - kill energy intensive apps
- study
 - lock time: -1
- film
  - lock time: -1
- presenter
  - no notifications

# Workspaces
Layout
Color of WS boxes
Color of bumblebee row

# Monitor arrangement

# Auto start
Use 'portable' profile
Autodetect
USB ID
IP addr
Possible extra interfaces (USB)
Charger
Displays

# Apps
- comm.
  - slack
  - mattermost
  - rocket chat

# Implementation
Python
IPC:socket
JSON
Buksidaemon
Buksictl
